import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Classe de gestion d album photo
 * @version 2.0
 */
public class AlbumPhoto {
	
	/**
	 * Nom de l album photo
	 */
	private String nom;
	
	
	/**
	 * Liste des photos de l albums
	 */
	private Map<String, Photo> listPhotos;
	
	/**
	 * Cree un album vide
	 * @param nom nom de l album
	 */
	public AlbumPhoto(String nom) {
		this.nom = nom;
		this.listPhotos = new HashMap<String, Photo>();
	}
	
	/**
	 * Cree un album deja rempli a l aide d un fichier
	 * @param nom nom de l album
	 * @param nomFichier nom du fichier servant a la creation de l album
	 */
	public AlbumPhoto(String nom, String nomFichier) throws IOException, WrongFileException{
		this.nom = nom;
		this.load(nomFichier);
	}
	
	/**
	 * cree et ajoute la pohto a l album
	 * @param nom nom de la personne
	 * @param mail mail de la personne
	 */
	public void ajouterPhoto(String path) {
		try {
			this.ajouterPhoto(new Photo(path));
		}catch(Exception e) {
			System.out.print("La personne "+ nom + " n a pas pu etre ajouter pour la raison suivante:\n    ");
			System.out.println(e);
		}
	}
	
	/**
	 * Ajoute la personne a la liste de l'event
	 * @param mrX personne a ajouter a la liste
	 */
	public void ajouterPhoto(Photo cliche) {
		if(!listPhotos.containsKey(cliche.getPath()))
			this.listPhotos.put(cliche.getPath(), cliche);
		else {
			System.out.println("La photo "+ cliche.getNom() + " n a pas pu etre ajouter pour la raison suivante: ");
			System.out.println("    la photo " + cliche.getPath() + " est deja dans l album");
		}
	}
	
	/**
	 * Enleve la photo dont le chemin est specifie sinon ne fait rien
	 * @param mail le chemin de la photo a retirer de l album
	 */
	public void retirerPhoto(String path) {
		this.listPhotos.remove(path);
	}
	
	/**
	 * mais a jour un album grace a un fichier
	 * @param nomFichier nom du fichier de sauvegarde
	 * @throws WrongFileException le fichier specifie ne correspond pas a une sauvegarde de cet album
	 */
	public void load(String nomFichier) throws IOException, WrongFileException {
		BufferedReader bIn = null;
		try {
			bIn = new BufferedReader(new FileReader(new File(nomFichier)));
			String ligne = bIn.readLine();
			if(!this.nom.equals(ligne))
				throw new WrongFileException(nomFichier, this.nom);
			this.listPhotos = new HashMap<String, Photo>();
			while((ligne = bIn.readLine()) != null) {
				ajouterPhoto(ligne);
			}
		}finally {
			try {
				if (bIn != null)
					bIn.close();
			}catch(IOException e){ System.out.println(e);
			}
		}
	}
	
	/**
	 * sauvegarde de l event sur un fichier
	 */
	public void save() {
		BufferedWriter bOut = null;
		try {
			bOut = new BufferedWriter(new FileWriter(new File(this.nom)));
			bOut.write(this.toString());
		}catch(FileNotFoundException e){ System.out.println(e);
		}catch(IOException e) { System.out.println(e);
		}finally {
			try {
				bOut.close();
			}catch(IOException e) {
				System.out.println(e);
			}
		}
	}
	
	/**
	 * retourne le nom de l'album 
	 * @return
	 */
	public String getnom() {
		return nom;
	}
	
	public String toString(){
		String s = new String(this.nom+"\n");
		for(Photo p : this.listPhotos.values()){
			s += p.toString()+"\n";
		}
		return s;
	}
}
