/**
 * Classe de test du projet PicView
 * @author Fabien Michael Theodore
 *
 */
public class TestProjet {
	public static void main(String[] args) {
		System.out.println("Test du Projet PicView: \n");
		PicView pics = new PicView();
		
		//Event
		System.out.println("########Event#########");
		System.out.println("Chargement des evenements ");
		pics.addEvent("Event_voyageCorse.txt");
		pics.addEvent("Event_mariageCecile.txt");
		
		System.out.println("\nAffichage de l evenement voyageCorse:");
		pics.printEvent("voyageCorse");
				
		System.out.println("\nAffichage de l evenement mariageCecile: ");
		pics.printEvent("mariageCecile");
		
		System.out.println("\nAffichage de la liste des evenements");
		pics.printEvenements();
		
		//Album
		System.out.println("\n########Album#########");
		System.out.println("Chargement des albums");
		pics.addAlbum("Album_Planetes.txt");
		
		System.out.println("\nAffichage de l album: ");
		pics.printAlbum("Planetes");
		
		System.out.println("\nAffichage de la liste d albums");
		pics.printAlbums();
		
		//AlbumEvent
		System.out.println("\n########AlbumEvent#########");
		System.out.println("Chargement des albums Evenementiels");
		pics.addAlbumEvent("AlbumEvent_mariageCecile.txt");
		pics.addAlbumEvent("AlbumEvent_voyageCorse.txt");
		
		System.out.println("\nAffichage des albums evenementiels");
		pics.printAlbumsEv();
		
		System.out.println("\nAffichage de l albumEv: ");
		pics.printAlbumEv("mariageCecile", 0);
		
		System.out.println("\nAffichage de l albumEv: ");
		pics.printAlbumEv("voyageCorse", 0);
		
	}
}
