import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Classe Principale du Projet d AlbumPhoto PIC VIEW
 * 
 * @version 2.0
 * @author Venier Fabien
 * @author Ducrocq Michael
 * @author Leblond Theodore
 */
public class PicView {
	/**
	 * Liste des extensions reconnu par PicView
	 */
	public static final String[] extension = {"jpg", "jpeg", "bmp", "gif", "png"};
	
	
	private Map<String,Event> listEvenement;
	private Map<String,AlbumPhoto> listAlbum;
	private Map<String,ArrayList<AlbumPhotoEvent>> listAlbumEv;
	
	/**
	 * Creation de la PicView
	 */
	public PicView() {
		this.listEvenement = new HashMap<String,Event>();
		this.listAlbum = new HashMap<String, AlbumPhoto>();
		this.listAlbumEv = new HashMap<String,ArrayList<AlbumPhotoEvent>>();
	}
	
	/**
	 * Retourne le type de fichier (Album, Event, AlbumEvent)
	 * @param path nom du fichier
	 * @return
	 */
	private String getType(String path) {
		StringTokenizer sT = new StringTokenizer(path, "_.");
		int nbToken = sT.countTokens();
		for(int i = 3; i < nbToken; i++) {
			sT.nextToken();
		}
		return sT.nextToken();
	}
	
	/**
	 * Recupere le nom du fichier sans sont type ou extention
	 * @param path nom du fichier
	 * @return
	 */
	private String getNom(String path) {
		StringTokenizer sT = new StringTokenizer(path, "_.");
		int nbToken = sT.countTokens();
		for(int i = 2; i < nbToken; i++) {
			sT.nextToken();
		}
		return sT.nextToken();
	}
	
	/**
	 *  Rajoute un evenement a l'ArrayList listEvenement 
	 * @param path nom du fichier
	 */
	public void addEvent(String path) {
		if(!this.getType(path).equals("Event")) {
			System.out.println("Mauvais fichier");
			return;
		}
		String nom = this.getNom(path);
		try {
			this.listEvenement.put(nom, new Event(nom, path));
		}catch(IOException e) {
			System.out.println(e);
		}catch(WrongFileException e) {
			System.out.println(e);
		}
	}
	
	/**
	 * Suprime un evenement de l'ArrayList listEvenement
	 * @param nom
	 */
	public void deleteEvent(String nom){
		Event ev = this.listEvenement.get(nom);
		if(ev == null)
			System.out.println(nom + " n existe pas");
		else
			listEvenement.remove(nom);
	}
	
	/**
	 * Affiche un Evenement (nom + personne)
	 * @param nom de l'evenement
	 */
	public void printEvent(String nom) {
		Event ev = this.listEvenement.get(nom);
		if(ev == null)
			System.out.println(nom + " n existe pas");
		else
			System.out.println(ev);		
	}
	
	/**
	 * Ajoute un Album à la hashmap listAlbum
	 * @param path d'acces a l'event
	 */
	public void addAlbum(String path) {
		if(!this.getType(path).equals("Album")) {
			System.out.println("Mauvais fichier");
			return;
		}
		String nom = this.getNom(path);
		try {
			this.listAlbum.put(nom, new AlbumPhoto(nom, path));
		}catch(IOException e) {
			System.out.println(e);
		}catch(WrongFileException e) {
			System.out.println(e);
		}
	}
	
	/**
	 * Supprime un Album à la hashmap listAlbum
	 * @param nom  d'acces a l'event
	 */
	public void deleteAlbum(String nom){
		AlbumPhoto alb = this.listAlbum.get(nom);
		if(alb == null)
			System.out.println(nom + " n existe pas");
		else
			listAlbum.remove(nom);
	}
	
	/**
	 * Affiche un Album (nom + Photo)
	 * @param nom  d'acces a l'event
	 */
	public void printAlbum(String nom) {
		AlbumPhoto alb = this.listAlbum.get(nom);
		if(alb == null)
			System.out.println(nom + " n existe pas");
		else
			System.out.println(alb);
			
	}
	
	/**
	 * Affiche un AlbumEvent (nom + Photo)
	 * @param nom de l'Album
	 * @param num de l'album
	 */
	public void printAlbumEv(String nom, int num) {
		ArrayList<AlbumPhotoEvent> currentValue = listAlbumEv.get(nom);
        AlbumPhotoEvent alb = currentValue.get(num);
    	if(alb == null)
    		System.out.println(nom + " n existe pas");
    	else
    		System.out.println(alb);
    }
	
	/**
	 * Rajoute un Album a la hashMap listAlbumEv 
	 * @param path d'acces a l'album
	 */
	public void addAlbumEvent(String path) {
		if(!this.getType(path).equals("AlbumEvent")) {
			System.out.println("Mauvais fichier");
			return;
		}
		String nom = this.getNom(path);
		Event ev = this.listEvenement.get(nom);
		try {
			if(!this.listAlbumEv.containsKey(nom)) {
				this.listAlbumEv.put(nom, new ArrayList<AlbumPhotoEvent>());
			}
			AlbumPhotoEvent albev = new AlbumPhotoEvent(nom, path, ev);
			if(this.listAlbumEv.get(nom).size()>0)
				albev.setNum(this.listAlbumEv.get(nom).size());
			this.listAlbumEv.get(nom).add(new AlbumPhotoEvent(nom, path, ev));
		}catch(IOException e) {
			System.out.println(e);
		}catch(WrongFileException e) {
			System.out.println(e);
		}
	}
	
	/**
	 * Suprime un albumEvent de la hashMap listAlbumEv
	 * @param nom de l'album
	 * @param num de l'album
	 */
	public void deleteAlbumEvent(String nom, int num){
		ArrayList<AlbumPhotoEvent> currentValue = this.listAlbumEv.get(nom);
        AlbumPhotoEvent alb = currentValue.get(num);
        if(alb == null)
    		System.out.println(nom + " n existe pas");
    	else
    		listAlbumEv.remove(nom);	
	}
	
	/**
	 * Affiche la liste des Events
	 */
	public void printEvenements() {
		System.out.println("Liste des evenements: ");
		for(String ev : listEvenement.keySet()){
			System.out.println(ev);
		}
	}
	
	/**
	 * Affiche la liste des Albums
	 */
	public void printAlbums() {
		System.out.println("Liste des Albums: ");
		for(String ev : listAlbum.keySet()){
			System.out.println(ev);
		}
	}
	
	/**
	 * Affiche la liste des AlbumsEvenementiel
	 */
	public void printAlbumsEv() {
		System.out.println("Liste des Albums evenementiels: ");
		for(String ev : listAlbumEv.keySet()){
			System.out.println(ev);
		}
	}
}
